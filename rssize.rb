require 'nokogiri'
require 'open-uri'
require 'rss/maker'

doc = Nokogiri::HTML(open("http://www.pzc.org.pl/index.php?option=com_content&task=category&sectionid=1&id=1&Itemid=2&limit=30&limitstart=0"));

news_rows = doc.xpath('//form[@name="adminForm"]//tr[@class]')

content = RSS::Maker.make('2.0') do |rss|
	rss.channel.title = "Polski Związek Curlingu"
	rss.channel.link = "http://pzc.org.pl"
	rss.channel.description = "PZC"

	news_rows.each do |row|
		a = row.xpath('.//a').first
		url = a.attribute('href').value
		title = a.content.strip

		item = rss.items.new_item
		item.title = title
		item.link = url

		date = Time.parse(row.xpath('.//td').first.content.strip)
		item.date = date
	end
end

puts content


